/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.client.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.NoRouteToHostException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pl.umk.mat.olaf.classes.Msg;
import pl.umk.mat.olaf.classes.UserData;

/**
 *
 * @author Aleksander
 */
public class ChatModelTest {
    
    private  ChatModel instance=null;
    UserData user =null;
    private Msg result;
    public ChatModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        user= new UserData("Olaf", "127.0.0.1", "0");
        instance= new ChatModel(user);
    }
    
    @After
    public void tearDown() throws InterruptedException {
    	Thread.sleep(200);
    }

    /**
     * Test of startConnection method, of class ChatModel.
     * @throws java.io.IOException because server is not running.
     */
    @Test
    public void testStartConnectionIfAddressIsNoaAvaliable() throws IOException, InterruptedException, NoRouteToHostException {
        Thread thread=new Thread(() ->
                {
            try {
                instance.startConnection("192.168.1.230");
            } catch (IOException ex) {
                //Logger.getLogger(ChatModelTest.class.getName()).log(Level.SEVERE, null, ex);
            }
                });
        thread.start();
        Thread.sleep(500);
        if(thread.isAlive()) 
        {
            thread.interrupt();
            assertTrue(true);
        }
        else
        {
            assertTrue(false);
        }
        
    }

    /**
     * Test of retrieveMessage method, of class ChatModel.
     */
    @Test
    public void testRetrieveMessage() throws IOException, ClassNotFoundException, InterruptedException {
        
        Msg expResult=new Msg(0, "Putin", "Dawaaj dawaaj");
        result=null;
        
                Thread thread=new Thread (() -> {
                     try {
                        
                        ServerSocket server=new ServerSocket(1200);
                        Socket accepted=server.accept();
                         OutputStream writer= accepted.getOutputStream();
                         ObjectOutputStream objectWriter= new ObjectOutputStream(writer);
                         
                         objectWriter.writeObject(new Msg(0, "Putin", "Dawaaj dawaaj"));
                         accepted.close();
                         server.close();
                        } catch (IOException ex) {
                Logger.getLogger(ChatModelTest.class.getName()).log(Level.SEVERE, null, ex);
            }
                });
                thread.start();
            
        
            Thread.sleep(200);
            instance.startConnection("127.0.0.1");
            result = instance.retrieveMessage();
            instance.closeConnection();
            thread.join();
            
        
        
        assertEquals(result.getMessage(), expResult.getMessage());
    }

    /**
     * Test of sendMessage method, of class ChatModel.
     */
    @Test
    public void testSendMessage() throws Exception {
        Msg expResult = new Msg(0, "Olaf", "It's me");
        
        Thread thread=new Thread (() -> {
                     try {
                        
                        ServerSocket server=new ServerSocket(1200);
                        Socket accepted=server.accept();
                         InputStream reader= accepted.getInputStream();
                         ObjectInputStream objectReader= new ObjectInputStream(reader);
                         
                         result= (Msg) objectReader.readObject();
                         accepted.close();
                         server.close();
                        } catch (IOException ex) {
                Logger.getLogger(ChatModelTest.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ChatModelTest.class.getName()).log(Level.SEVERE, null, ex);
            }
                });
        thread.start();
        Thread.sleep(200);
        instance.startConnection("127.0.0.1");
        instance.sendMessage(expResult);
        instance.closeConnection();
        thread.join();
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(expResult.getMessage(),result.getMessage());
    }

    /**
     * Test of closeConnection method, of class ChatModel.
     */
    @Test
    public void testCloseConnection() throws Exception {
        Msg expResult = new Msg(0, "Olaf", "It's me");
        
        Thread thread=new Thread (() -> {
                     try {
                        
                        ServerSocket server=new ServerSocket(1200);
                        Socket accepted=server.accept();
                         InputStream reader= accepted.getInputStream();
                         ObjectInputStream objectReader= new ObjectInputStream(reader);
                         
                         result= (Msg) objectReader.readObject();
                         accepted.close();
                         server.close();
                        } catch (IOException ex) {
                Logger.getLogger(ChatModelTest.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ChatModelTest.class.getName()).log(Level.SEVERE, null, ex);
            }
                });
        thread.start();
        Thread.sleep(200);
        instance.startConnection("127.0.0.1");
        instance.sendMessage(expResult);
        instance.closeConnection();
        thread.join();
        ServerSocket socket=null;
        try
        {
            socket=new ServerSocket(1200);
        }
        catch(IOException e)
        {
            assertTrue(false);
        }
        socket.close();
        
    }
    
}
