/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.lobby.model;

import java.util.ArrayList;
import java.lang.String;
import java.lang.Integer;
import pl.umk.mat.olaf.classes.UserData;
import pl.umk.mat.olaf.lobby.lobbythread.LobbyThread;

/**
 * The Class LobbyModel.
 *
 * @author olaf
 */
public class LobbyModel {
    
    /** The rooms. */
    private ArrayList<Integer> rooms=null;
    
    /** The port. */
    private final Integer port=1200;
    
    /** The server address. */
    private String serverAddress;
    
    /** The thread. */
    private LobbyThread thread;
    
    /**
     * Instantiates a new lobby model.
     */
    public LobbyModel()
    {
        thread=new LobbyThread(port);
    }
    
    /**
     * Function start new thread, wait for him and set all requied info.
     *
     * @throws InterruptedException the interrupted exception
     */
    public void findServerAddressAndReceiveInfo() throws InterruptedException
    {
        thread.start();
        thread.join();
        serverAddress=thread.getServerAddress();
        rooms=thread.getServerInfo();
    }
    
    /**
     * Function creates UserData object (without ObjectOutputStream).
     *
     * @param nickname user's nickname
     * @param roomID user's room
     * @return UserData
     */
    public UserData getUserInfo(String nickname, Integer roomID)
    {
        UserData temp= new UserData((nickname), roomID, null);
        temp.setServerAddress(serverAddress);
        return temp;
    }
    
    /**
     * Function returns 0 if there is no rooms or the lowest free key.
     *
     * @return free room id
     */
    public Integer getTheLowestAvailableRoomNumber()
    {
        if(rooms==null || rooms.isEmpty()) return 0;
        else 
        {
            int i=0;
            for(Integer number : rooms)
            {
                if(number==i) i++;
                else break;
            }
            return i;
        }
    }

    /**
     * Gets the all room ids.
     *
     * @return the all room ids
     */
    public ArrayList<Integer> getAllRoomIDs()
    {
        return rooms;
    }
    
    /**
     * Gets the server address.
     *
     * @return the server address
     */
    public String getServerAddress()
    {
        return this.serverAddress;
    }
}
