/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.client.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

import pl.umk.mat.olaf.classes.Msg;
import pl.umk.mat.olaf.classes.UserData;

/**
 * The Class ChatModel.
 *
 * @author olaf
 */
public class ChatModel {

    /** The user. */
    private final UserData user;
    
    /** The client socket. */
    private Socket clientSocket;
    
    /** The port. */
    private final Integer port= 1200;
    
    /** The object reader. */
    private ObjectInputStream objectReader=null;
    
    /** The reader. */
    private InputStream reader=null;
    
    /** The object writer. */
    private ObjectOutputStream objectWriter=null;
    
    /** The writer. */
    private OutputStream writer=null;
    
    /**
     * Instantiates a new chat model.
     *
     * @param user the user
     */
    public ChatModel(UserData user)
    {
        this.user=user;
    }
    
    /**
     * Start connection.
     *
     * @param address to connect with
     * @throws IOException when opening the socket was impossible.
     */
    public void startConnection(String address) throws IOException
    {
        clientSocket=new Socket(address,port); 
    }
    
    /**
     * Retrieve message.
     *
     * @return the msg
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ClassNotFoundException the class not found exception
     */
    public Msg retrieveMessage() throws IOException, ClassNotFoundException
    {
        if(clientSocket!=null)
        {
            if(objectReader==null)
            {
                reader=clientSocket.getInputStream();
                objectReader= new ObjectInputStream(reader);
            }
            Msg message;
            
            message= (Msg) objectReader.readObject();
            
            return message;
        }
        else throw new IOException("Client socket is not open!");
    }
    
    /**
     * Send message.
     *
     * @param message the message
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void sendMessage(Msg message) throws IOException
    {
        if(clientSocket!=null)
        {
            if(objectWriter==null)
            {
                writer= clientSocket.getOutputStream();
                objectWriter=new ObjectOutputStream(writer);
            }
            objectWriter.writeObject(message);
        }
        else throw new IOException("Client socket is not open!");
    }
    
    /**
     * Close connection.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void closeConnection() throws IOException
    {
        if(!clientSocket.isClosed()) clientSocket.close();
    }
}
