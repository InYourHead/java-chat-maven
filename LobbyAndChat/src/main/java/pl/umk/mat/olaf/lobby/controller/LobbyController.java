/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.lobby.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.umk.mat.olaf.classes.UserData;
import pl.umk.mat.olaf.client.MVC.ChatMVC;
import pl.umk.mat.olaf.lobby.model.LobbyModel;
import pl.umk.mat.olaf.lobby.view.LobbyView;

/**
 * The Class LobbyController.
 *
 * @author olaf
 */
public class LobbyController {
    
    /** The view. */
    LobbyView view;
    
    /** The model. */
    LobbyModel model;
    
    /** The search thread. */
    Thread searchThread;
    
    /** The user. */
    UserData user;
    
    /** The selected. */
    boolean selected=false;
    
    /**
     * Instantiates a new lobby controller.
     *
     * @param model the model
     * @param view the view
     */
    public LobbyController(LobbyModel model, LobbyView view)
    {
        this.model=model;
        this.view=view;
        
        view.setExitActionListener(new WindowAdapter() {
            @Override
           public void windowClosing(WindowEvent e)
           {
               if(view.isSearchRunning())
               {
                   
                   searchThread.interrupt();
               }
               view.dispose();
           }           
        });
        view.addJoinBoxActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(selected==true)
                {
                    if(!view.getNicknameTyped().isEmpty())
                    {
                        user=model.getUserInfo(view.getNicknameTyped(), view.getSelectedItem());
                        
                        view.setVisible(false);
                        
                        System.out.println("Lets start");
                        
                            
                            Thread thread = new Thread(()-> {ChatMVC client= new ChatMVC();
                            ChatMVC.main(new String[]{user.getUserNickname(),user.getServerAddress(), String.format("%d", user.getRoomID())});
                            });
                            thread.start();
                        try {
                            thread.join();

                        } catch (InterruptedException ex) {
                            Logger.getLogger(LobbyController.class.getName()).log(Level.SEVERE, null, ex);
                       }
                            view.dispose(); 
                    }
                    else 
                    {
                        view.showErrorMessage("Nickname must be typed.");
                    }
                }
                else
                {
                    view.showErrorMessage("Select room.");
                }
            }
        });
        
        view.addListSelectedActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selected=true;
            }
        });
        
        /**
         * Start searching server in new thread
         */
        searchThread=new Thread(() -> {
            try {
                model.findServerAddressAndReceiveInfo();
                
                if(!model.getServerAddress().equals("0.0.0.0"))
                {
                    ArrayList<Integer> list=model.getAllRoomIDs();
                    list.add(model.getTheLowestAvailableRoomNumber());
                
                    view.addListOfRooms(list.toArray(new Integer[list.size()]));
                
                    view.setJoinButtonLabel("JOIN");
                    view.enableJoinButton();
                }
                else
                {
                        view.setJoinButtonLabel("Server not exists");
                }
            }catch (InterruptedException ex) {
                view.showErrorMessage(ex.getMessage());
                Logger.getLogger(LobbyController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        searchThread.start();        
    }
    
}
