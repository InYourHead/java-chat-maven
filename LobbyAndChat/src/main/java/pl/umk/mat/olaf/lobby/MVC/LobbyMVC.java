/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.lobby.MVC;

import pl.umk.mat.olaf.lobby.controller.LobbyController;
import pl.umk.mat.olaf.lobby.model.LobbyModel;
import pl.umk.mat.olaf.lobby.view.LobbyView;

/**
 * The Class LobbyMVC.
 *
 * @author olaf
 */
public class LobbyMVC {
    
    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args)
	{
                System.setProperty("java.net.preferIPv4Stack" , "true");
		LobbyView theView= new LobbyView();
		LobbyModel theModel= new LobbyModel();
		new LobbyController(theModel,theView);
			theView.setVisible(true);
	}
    
}
