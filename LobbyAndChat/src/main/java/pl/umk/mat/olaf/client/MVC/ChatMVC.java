/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.client.MVC;

import pl.umk.mat.olaf.client.controller.ChatController;
import pl.umk.mat.olaf.client.model.ChatModel;
import pl.umk.mat.olaf.client.view.ChatView;
import pl.umk.mat.olaf.classes.UserData;

/**
 * The Class ChatMVC.
 *
 * @author olaf
 */
public class ChatMVC {
    
    /**
     * The main method.
     *
     * @param argvs the arguments
     */
    public static void main(String [] argvs)
	{
                
		ChatView theView= new ChatView();
                
		ChatModel theModel= new ChatModel(new UserData(argvs[0],argvs[1],argvs[2]));
                new ChatController(theModel,theView, new UserData(argvs[0],argvs[1],argvs[2]));
                theView.setVisible(true);
	}

}
