/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.client.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.umk.mat.olaf.client.model.ChatModel;
import pl.umk.mat.olaf.client.view.ChatView;
import pl.umk.mat.olaf.classes.Msg;
import pl.umk.mat.olaf.classes.UserData;

/**
 * The Class ChatController.
 *
 * @author olaf
 */
public class ChatController {
    
    /** The model. */
    private ChatModel model;
    
    /** The view. */
    private ChatView view;
    
    /** The user. */
    private UserData user;
    
    /** The threads. */
    private ArrayList<Thread> threads= new ArrayList<>();
    
    /** The can i send. */
    private boolean canISend=true;
    
    /**
     * Instantiates a new chat controller.
     *
     * @param model the model
     * @param view the view
     * @param user the user
     */
    public ChatController(ChatModel model, ChatView view, UserData user)
    {
        this.model=model;
        this.view=view;
        this.user=user;
        /**
         * Add all listeners
         */
        view.addSendButtonListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!view.getChatTextFieldMessage().isEmpty())
                {
                    
                        try {
                            model.sendMessage(new Msg(user.getRoomID(), user.getUserNickname(),view.getChatTextFieldMessage()));
                            view.setChatTextFieldMessage("");
                        } catch (IOException ex) {
                            view.showErrorMessage("Nie można wysłać wiadomości!");
                            Logger.getLogger(ChatController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                                
                }
            }
        });
        view.setExitActionListener(new WindowAdapter() {
             @Override
           public void windowClosing(WindowEvent e)
           {
            	 try {
					model.sendMessage(new Msg(user.getRoomID(),user.getUserNickname(),"disconnect",null));
            	 } catch (IOException e1) {
					e1.printStackTrace();
				}
            	 
              for(Thread thread : threads)
              {
                  if(thread.isAlive())
                      thread.interrupt();
              }
              view.dispose();
           }           
});
        view.turnOnResizeTableListener();
        /**
         * Start client application
         */
            if(canISend)
            {
               
                /**
                 * send join message
                 */
                    
                            try {
                            model.startConnection(user.getServerAddress());
                            } catch (IOException ex) {
                        canISend=false;
                        Logger.getLogger(ChatController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                        
            try {
                model.sendMessage(new Msg(user.getRoomID(),user.getUserNickname(),"join", null));
            } catch (IOException ex) {
            	canISend=false;
                Logger.getLogger(ChatController.class.getName()).log(Level.SEVERE, null, ex);
            }
                    
            }
            if(canISend)
            {
                /**
                 * start listening!
                 */
                
                    Thread thread=new Thread(() -> {
                    while(true)
                    {
                        try {
                            Msg message=model.retrieveMessage();
                            view.addNewMessage(new String[]{message.getSender(), message.getMessage()});
                        } catch (IOException ex) {
                            Logger.getLogger(ChatController.class.getName()).log(Level.SEVERE, null, ex);
                            break;
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(ChatController.class.getName()).log(Level.SEVERE, null, ex);
                            break;
                        }
                    }
                });
                thread.start();

                view.setButtonEnabled();
                
            }   
    }
   
}
