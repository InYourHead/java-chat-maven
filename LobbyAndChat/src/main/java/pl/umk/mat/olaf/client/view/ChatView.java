/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.client.view;

import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 * The Class ChatView.
 *
 * @author Aleksander
 */
public class ChatView extends javax.swing.JFrame {

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5294436196345268430L;
	/**
     * Creates new form Chat.
     */
    public ChatView() {
        
        initComponents();
        tableModel = (DefaultTableModel) ChatTable.getModel();
        ChatTable.setTableHeader(null);
        jScrollPane2.setColumnHeader(null);
        this.getRootPane().setDefaultButton(ChatSendButton);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        ChatTextField = new javax.swing.JTextField();
        ChatSendButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        ChatTable = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(394, 325));

        ChatTextField.setMinimumSize(new java.awt.Dimension(273, 29));

        ChatSendButton.setText("Send");
        ChatSendButton.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(ChatTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ChatSendButton))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(ChatTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(ChatSendButton))
        );

        ChatTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "From", "Message"

            }
        ));
        ChatTable.setEditingColumn(0);
        ChatTable.setEditingRow(0);
        ChatTable.setIntercellSpacing(new java.awt.Dimension(0, 0));
        ChatTable.setRowSelectionAllowed(false);
        ChatTable.setShowHorizontalLines(false);
        ChatTable.setShowVerticalLines(false);
        jScrollPane2.setViewportView(ChatTable);
        ChatTable.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(21, 21, 21))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    /**
     * Set new Exit action listener.
     *
     * @param adapter new WindowAdapter
     */
   public void setExitActionListener(WindowAdapter adapter)
   {
       this.addWindowListener(adapter);
   }
   
   /**
    * Set new button pressed action listener.
    *
    * @param listener new Actionlistener
    */
   public void addSendButtonListener(ActionListener listener)
   {
       ChatSendButton.addActionListener(listener);
   }
   
   /**
    * Turn on resize table listener.
    */
   public void turnOnResizeTableListener()
   {
	   ChatTable.addComponentListener(new ComponentAdapter()
	{
		@Override
		public void componentResized(ComponentEvent e)
		{
			ChatTable.scrollRectToVisible(ChatTable.getCellRect(ChatTable.getRowCount()-1, 0, true));
		}
	});
   }
   
   /**
    * Get User's message.
    *
    * @return typed message (String)
    */
   public String getChatTextFieldMessage()
   {
       return ChatTextField.getText();
   }
   
   /**
    * Set text field message.
    *
    * @param message the new chat text field message
    */
   public void setChatTextFieldMessage(String message)
   {
       ChatTextField.setText(message);
   }
   
   /**
    * Show new message in the chat window.
    *
    * @param message the message
    */
   public void addNewMessage(String[] message)
   {
       tableModel.addRow(message);
   }
   
   /**
    * Show new pop-up window with message.
    *
    * @param message to show
    */
   public void showErrorMessage(String message)
   {
       JOptionPane.showMessageDialog(null, message);
   }
   
   /**
    * Set the window title.
    *
    * @param title the new title
    */
   public void setTitle(String title)
   {
       this.setTitle(title);
   }
   
   /**
    * Sets the button status.
    */
   public void setButtonEnabled()
   {
       this.ChatSendButton.setEnabled(true);
   }
    
    /** The table model. */
    private DefaultTableModel tableModel;
    
    /** The Chat send button. */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ChatSendButton;
    private javax.swing.JTable ChatTable;
    private javax.swing.JTextField ChatTextField;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
}
