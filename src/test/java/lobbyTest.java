/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.TreeMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.umk.mat.olaf.classes.Msg;
import pl.umk.mat.olaf.classes.Room;
import pl.umk.mat.olaf.lobby.lobbythread.LobbyThread;
import pl.umk.mat.olaf.server.serverthread.ServerThread;

/**
 *
 * @author olaf
 */
public class lobbyTest {

	private Msg receivedMessage;
	Msg message = new Msg(1, "Olaf", "join", null);
	TreeMap<Integer, Room> rooms = new TreeMap<>();
	ArrayList<Integer> roomsInfo = new ArrayList<>();
	private Socket clientSocket;

	public lobbyTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}
	@Test
	public void getServerAddressAndRoomsList()
			throws IOException, InterruptedException {
		/**
		 * prepare one connected client
		 */

		ServerThread myServer = new ServerThread(rooms, 1200);
		myServer.start();

		LobbyThread lobby = new LobbyThread(1200);
		/**
		 * now send special message from another socket
		 */
		System.out.println("Zaczynamy!");
		lobby.findServerAddressAndReceiveInfo();
		/**
		 * wait for stop
		 */
		lobby.join();
		/**
		 * retrieve informations
		 */
		String serverAddress = lobby.getServerAddress();
		Assert.assertTrue(serverAddress.equals("127.0.0.1"));
		roomsInfo = lobby.getServerInfo();
		boolean isEmpty = roomsInfo.isEmpty();
		Assert.assertTrue(roomsInfo.isEmpty());

		 myServer.stopServerSocket();
		 myServer.terminateAllSubthreads();
	}
	// TODO add test methods here.
	// The methods must be annotated with annotation @Test. For example:
	//
	// @Test
	// public void hello() {}
}
