import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.umk.mat.olaf.classes.Msg;
import pl.umk.mat.olaf.classes.UserData;
import pl.umk.mat.olaf.client.model.ChatModel;
import pl.umk.mat.olaf.server.model.ServerModel;

public class ServerAndClientTests {

	private ServerModel server;
	private ChatModel client;
	private UserData user;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		user= new UserData("Olaf","127.0.0.1","0");
		server = new ServerModel();
		client = new ChatModel(user);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRecivingAndSendingMessages() throws IOException, ClassNotFoundException, InterruptedException {

		
		//server.startServerSocket();
		server.retrieveAndSendMessage();
		
		client.startConnection("127.0.0.1");
		Thread.sleep(100);
		client.sendMessage(new Msg(0,"Olaf","join",null));
		Msg message=client.retrieveMessage();
		Assert.assertTrue(message.getMessage().equals("Olaf joined to the room."));
		
		Msg tempMessage=new Msg(0, "Olaf", "Hello");
		client.sendMessage(tempMessage);
		Assert.assertTrue(client.retrieveMessage()
		.getMessage().equals("Hello"));
		
		server.terminateAllthreads();
		client.closeConnection();
		
	}

}
