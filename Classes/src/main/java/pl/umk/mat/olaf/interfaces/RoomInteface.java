/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.interfaces;

import pl.umk.mat.olaf.classes.UserData;
import java.util.ArrayList;

/**
 * The Interface RoomInteface.
 *
 * @author Aleksander
 */
public interface RoomInteface {
    
    /**
     * Add user to the room.
     *
     * @param user UserData
     */
    public void addUser(UserData user);
    /**
     * Remove user from the room.
     * @param user UserData
     */
    public void removeUser(UserData user);
    /**
     * Return list of existing users.
     * @return ArrayList users.
     */
    public ArrayList<UserData> returnUsersList();
    
    /**
     * Return true if user exists in the room, false if there is no user.
     *
     * @param user to find
     * @return true or false.
     */
    public boolean isUserExisting(UserData user);
    /**
     * Check if room is empty.
     * @return true if room is empty or false if is not empty.
     */
    public boolean isEmpty();
    
}
