/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.classes;


import java.io.ObjectOutputStream;
import pl.umk.mat.olaf.interfaces.UserDataInterface;

// TODO: Auto-generated Javadoc
/**
 * The Class UserData.
 *
 * @author Aleksander
 */
public class UserData implements UserDataInterface{

    /** The server address. */
    private String serverAddress;
    
    /** The nickname. */
    private final String nickname;
    
    /** The room id. */
    private final Integer roomID;
    
    /** The writer. */
    private final ObjectOutputStream writer;
    
    /**
     * Instantiates a new user data.
     *
     * @param nickname the nickname
     * @param serverAddress the server address
     * @param roomID the room id
     */
    public UserData(String nickname, String serverAddress, String roomID)
    {
        this.nickname=nickname;
        this.roomID= Integer.parseInt(roomID);
        this.serverAddress= serverAddress;
        this.writer=null;
    }
    
    /**
     * Instantiates a new user data.
     *
     * @param nickname the nickname
     * @param roomID the room id
     * @param writer the writer
     */
    public UserData(String nickname,Integer roomID, ObjectOutputStream writer)
    {
        this.nickname=nickname;
        this.roomID=roomID;
        this.writer=writer;
    }
    
    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.UserDataInterface#getRoomID()
     */
    @Override
    public Integer getRoomID() {
        return this.roomID;
    }

    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.UserDataInterface#getUserNickname()
     */
    @Override
    public String getUserNickname() {
        return this.nickname;
    }

    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.UserDataInterface#getUserObjectInputStream()
     */
    @Override
    public ObjectOutputStream getUserObjectInputStream() {
        return this.writer;
    }

    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.UserDataInterface#setServerAddress(java.lang.String)
     */
    @Override
    public void setServerAddress(String address) {
        this.serverAddress=address;
    }

    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.UserDataInterface#getServerAddress()
     */
    @Override
    public String getServerAddress() {
        return this.serverAddress;
    }
}
