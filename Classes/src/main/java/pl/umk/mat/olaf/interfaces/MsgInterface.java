/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.interfaces;

import java.io.Serializable;

/**
 *Interface used to implements Message classes.
 * @author Aleksander
 */
public interface MsgInterface extends Serializable {
    
    /**
     * Returns user's message.
     * @return message in String format.
     */
    public String getMessage();
    /**
     * Returns sender nickname.
     * @return sender nickname in string format.
     */
    public String getSender();
    
    /**
     * Returns special message.
     *
     * @return special message as String.
     */
    public String getSpecialMessage();
    /**
     * Returns room ID.
     * @return roomID as Integer
     */
    public Integer getRoomID();
  
}
