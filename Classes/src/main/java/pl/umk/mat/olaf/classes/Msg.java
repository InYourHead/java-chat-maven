/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.classes;

import pl.umk.mat.olaf.interfaces.MsgInterface;

// TODO: Auto-generated Javadoc
/**
 * The Class Msg.
 *
 * @author Aleksander
 */
public class Msg implements MsgInterface{
    
    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8770205724656623695L;

	/** The message. */
    private final String message;
    
    /** The special message. */
    private final String specialMessage;
    
    /** The sender. */
    private final String sender;
    
    /** The room id. */
    private final Integer roomID;

    /**
     * Instantiates a new msg.
     *
     * @param roomID the room id
     * @param sender the sender
     * @param message the message
     */
    public Msg(Integer roomID, String sender, String message)
    {
        this.roomID=roomID;
        this.sender=sender;
        this.message=message;
        this.specialMessage=null;
    }
    
    /**
     * Instantiates a new msg.
     *
     * @param roomID the room id
     * @param sender the sender
     * @param specialMessage the special message
     * @param message the message
     */
    public Msg(Integer roomID, String sender, String specialMessage, String message)
    {
        this.roomID=roomID;
        this.message=message;
        this.sender=sender;
        this.specialMessage=specialMessage;
    }
    
    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.MsgInterface#getMessage()
     */
    @Override
    public String getMessage() {
        return this.message;
    }

    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.MsgInterface#getSender()
     */
    @Override
    public String getSender() {
        return this.sender;
    }

    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.MsgInterface#getSpecialMessage()
     */
    @Override
    public String getSpecialMessage() {
        return this.specialMessage;
    }

    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.MsgInterface#getRoomID()
     */
    @Override
    public Integer getRoomID() {
        return this.roomID;
    }
    
}
