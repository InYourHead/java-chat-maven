/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.classes;

import java.util.ArrayList;
import pl.umk.mat.olaf.interfaces.RoomInteface;

/**
 * The Class Room.
 *
 * @author olaf
 */
public class Room implements RoomInteface{

    /** The users. */
    private ArrayList<UserData> users= new ArrayList<>();
    
    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.RoomInteface#addUser(pl.umk.mat.olaf.classes.UserData)
     */
    @Override
    public void addUser(UserData user) {  	
        this.users.add(user);
    }

    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.RoomInteface#removeUser(pl.umk.mat.olaf.classes.UserData)
     */
    @Override
    public void removeUser(UserData user) {
    	System.out.println("Before deleting...");
    	for(UserData user1 : users)
    	{
    		System.out.println(!user1.equals(null));
    	}
        users.remove(user);
        System.out.println("After deleting...");
    	for(UserData user1 : users)
    	{
    		System.out.println(!user1.equals(null));
    	}
    }

    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.RoomInteface#isUserExisting(pl.umk.mat.olaf.classes.UserData)
     */
    @Override
    public boolean isUserExisting(UserData user) {
        return users.contains(user);
    }

    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.RoomInteface#isEmpty()
     */
    @Override
    public boolean isEmpty() {
        return this.users.isEmpty();
    }

    /* (non-Javadoc)
     * @see pl.umk.mat.olaf.interfaces.RoomInteface#returnUsersList()
     */
    @Override
    public ArrayList<UserData> returnUsersList() {
    	
        return this.users;
    }
    
}
