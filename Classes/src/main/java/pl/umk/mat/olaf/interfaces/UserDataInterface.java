/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.interfaces;

import java.io.ObjectOutputStream;

/**
 * The Interface UserDataInterface.
 *
 * @author Aleksander
 */
public interface UserDataInterface {
    
    /**
     * Get room id.
     *
     * @return Integer
     */
    public Integer getRoomID();
    
    /**
     * Get user nickname.
     *
     * @return String
     */
    public String getUserNickname();
    
    /**
     * Return active Object Output Stream.
     *
     * @return the user object input stream
     */
    public ObjectOutputStream getUserObjectInputStream();
    /**
     * Function set server address.
     * @param address server address
     */
    public void setServerAddress(String address);
    /**
     * Function returns saved server address or null, if address was not declared.
     * @return String
     */
    public String getServerAddress();
    
}
