/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.TreeMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.umk.mat.olaf.classes.Msg;
import pl.umk.mat.olaf.classes.Room;
import pl.umk.mat.olaf.server.serverthread.ServerThread;

/**
 *
 * @author olaf
 */
public class serverTest {

	private Socket clientSocket;
	Msg message = new Msg(0, "Olaf", "join", null);
	TreeMap<Integer, Room> rooms = new TreeMap<>();

	Msg receivedMessage;

	public serverTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void receiveWelcomeMessage()
			throws IOException, InterruptedException {
		ServerThread myServer = new ServerThread(rooms, 1200);
		myServer.start();
		Thread.sleep(200);
		clientSocket = new Socket("localhost", 1200);
		ObjectOutputStream writer = new ObjectOutputStream(
				clientSocket.getOutputStream());
		writer.writeObject(message);
		ObjectInputStream reader = new ObjectInputStream(
				clientSocket.getInputStream());
		try {
			receivedMessage = (Msg) reader.readObject();
		} catch (ClassNotFoundException ex) {
			System.out.println(ex.getMessage());
Assert.fail();
		}

		Assert.assertTrue(receivedMessage.getMessage()
				.equals("Olaf joined to the room."));
		myServer.stopServerSocket();
		myServer.terminateAllSubthreads();
		Thread.sleep(200);
	}
	@Test
	public void getInfoAboutUserDisconnect()
			throws IOException, InterruptedException {
		ServerThread myServer = new ServerThread(rooms, 1200);
		myServer.start();
		clientSocket = new Socket("localhost", 1200);

		ObjectOutputStream writer = new ObjectOutputStream(
				clientSocket.getOutputStream());
		writer.writeObject(message);
		writer.writeObject(new Msg(1, "Olaf", "disconnect", null));
		
		myServer.stopServerSocket();
		myServer.terminateAllSubthreads();
		Thread.sleep(200);
	}
}
