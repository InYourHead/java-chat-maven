/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.server.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.umk.mat.olaf.server.model.ServerModel;
import pl.umk.mat.olaf.server.view.ServerView;

/**
 * Class used to connect ServerModel and ServerView.
 *
 * @author Aleksander
 */
public class ServerController {
    
    /** The model. */
    @SuppressWarnings("unused")
	private ServerModel model;
    
    /** The view. */
    private ServerView view;
    
    /** The first time. */
    private boolean firstTime=true;
    
    /**
     * Instantiates a new server controller.
     *
     * @param model the model
     * @param view the view
     */
    public ServerController(ServerModel model, ServerView view)
    {
        this.model=model;
        this.view=view;
        
        this.view.setExitActionListener(new WindowAdapter() {
                @Override
            public void windowClosing(WindowEvent e)
            {
                System.out.println("Closed");
                try {
                        if(view.isServerRunning())
                            model.terminateAllthreads();
                    } catch (IOException ex) {
                        view.showErrorMessage("Błąd przy zamykaniu socketa!");
                        Logger.getLogger(ServerController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ServerController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                e.getWindow().dispose();
            }
                });
        
        this.view.addStartStopButtonListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(firstTime==true)
                {
                    view.setButtonLabel("STOP");
                    firstTime=false;
                    view.setIPTextFieldLabel(model.getCurrentEnvironmentNetworkIp());
                    view.setBroadcastTextFieldLabel(model.getCurrentEnvironmentNetworkBroadcastAddress());
                    
                    
                  Thread thread= new Thread (()->{
                        try {
                            model.retrieveAndSendMessage();
                            
                                 
                        } catch (IOException ex) {
                            Logger.getLogger(ServerController.class.getName()).log(Level.SEVERE, null, ex);
                        
                    }
                        });
                        thread.start();
                }
                else
                {
                    System.exit(0);
                    //TO DO
                   //this.processWindowEvent(new WindowEvent(view.get, WindowEvent.WINDOW_CLOSING));
                   
                }
            }
        
        });
    }


}
