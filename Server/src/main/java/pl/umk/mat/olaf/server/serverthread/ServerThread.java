/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.server.serverthread;

import pl.umk.mat.olaf.classes.Msg;
import pl.umk.mat.olaf.classes.Room;
import pl.umk.mat.olaf.classes.UserData;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class is used to send and receive messages in Msg format.
 *
 * @author olaf
 */
public class ServerThread extends Thread{
    
    /** The server socket. */
    private ServerSocket serverSocket=null;
    
    /** The rooms. */
    private TreeMap<Integer, Room> rooms;
    
    /** The syncho object. */
    private final Object synchoObject=new Object();
    
    /** The port. */
    private final Integer port;
    
    /** The message. */
    private Msg message;
    
    /** The exception user. */
    private UserData exceptionUser;
    
    /** The thread. */
    private Thread thread;
    
    /** The threads. */
    ArrayList<Thread> threads= new ArrayList<>();
    
    /** Boolean can i run. */
    private boolean canIRun=true;
    
    /**
     * Constructor.
     *
     * @param rooms HashMap consisting informations about rooms
     * @param port ServerSocket port (Integer)
     */
    public ServerThread(TreeMap<Integer, Room> rooms, Integer port)
    {
        this.rooms=rooms;
        this.port=port;
    }
    
    /**
     * creates new ServerSocket listening on port given earlier.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void startServerSocket() throws IOException
    {
        serverSocket=new ServerSocket(port);
    }
    
    /**
     * Stop server socket.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws InterruptedException the interrupted exception
     */
    public void stopServerSocket() throws IOException, InterruptedException
    {
        synchronized(synchoObject)
        {
        	terminateAllSubthreads();
        }   
        Thread.sleep(10);
        serverSocket.close();
    }
    
    /**
     * Can i run.
     *
     * @return true, if successful
     */
    public boolean canIRun()
    {
        return this.canIRun;
    }
    /**
     * terminate all generated threads.
     */
    public void terminateAllSubthreads()
    {
        threads.stream().forEach((tempThread) -> {
            tempThread.interrupt();
        });
    }
    
    /**
     * It recives and sending messages to existing rooms (users).
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void recieveAndSendMessage() throws IOException 
    {
        while(canIRun)
        {
            /**
             * Accepts incoming connection
             */
            Socket acceptSocket=serverSocket.accept();
            
            /**
             * Creates new thread to serve
             */
            thread=new Thread(() -> {
                
            UserData user=null;
            InputStream reader = null;
            ObjectInputStream objectReader = null;
            @SuppressWarnings("unused")
			OutputStream writer=null;
            @SuppressWarnings("unused")
			ObjectOutputStream objectWriter=null;
            
            try {
                /**
                 * Receiving first message from client
                 */
                reader= acceptSocket.getInputStream();
                objectReader= new ObjectInputStream(reader);
                message=(Msg)objectReader.readObject();
                user=new UserData(message.getSender(), message.getRoomID(), new ObjectOutputStream(acceptSocket.getOutputStream()));
                
            } catch (IOException ex) {
                
                try {
                                    if(!acceptSocket.isClosed()) acceptSocket.close();
                             } catch (IOException e) {
                                    System.out.println("Error while closing socket");
                                    Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
                             }
                System.out.println("Error with getting InputStream or readingObject! Closing socket...");
                Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
                
            } catch (ClassNotFoundException ex) {
                try {
                                    if(!acceptSocket.isClosed()) acceptSocket.close();
                             } catch (IOException e) {
                                    System.out.println("Error while closing socket");
                                    Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
                             }
                System.out.println("Class Msg not exists!");
                Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
            }
                if(message.getSpecialMessage()!=null)
                {
                    /**
                     * send info about joining the user to chatroom
                     */
                    if(message.getSpecialMessage().equals("join"))
                    {
                        
                            System.out.println("New user connected!");
                            /**
                             * adding new user to the Room
                             */
                            synchronized(synchoObject)
                            {
                                if(!rooms.containsKey(message.getRoomID()))
                                    rooms.put(message.getRoomID(), new Room());
                                rooms.get(user.getRoomID()).addUser(user);
                            }
                
                            
                        for(UserData tempUser : rooms.get(user.getRoomID()).returnUsersList())
                        {
                            try {
                                tempUser.getUserObjectInputStream().writeObject(new Msg(null,null, null,user.getUserNickname()+" joined to the room."));
                            } catch(IOException ex)
                            {
                                System.out.println("User: " + tempUser.getUserNickname() + "Is not available!");
                                synchronized(synchoObject)
                                {
                                    if(rooms.get(user.getRoomID())!= null) rooms.get(user.getRoomID()).removeUser(tempUser);
                                    System.out.println("User "+tempUser.getUserNickname() + "has been removed.");
                                }
                            }
                            System.out.println("Sended to:" + tempUser.getUserNickname());
                        }
                        System.out.println("Info about new user sended");
                    }
                    else if(message.getSpecialMessage().equals("info"))
                    {
                        try{
                            user.getUserObjectInputStream().writeObject(new ArrayList(rooms.keySet()));
                        } catch (IOException ex) {
                            System.out.println("Error while sending rooms list");
                            Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
                             try {
                                    if(!acceptSocket.isClosed()) acceptSocket.close();
                             } catch (IOException e) {
                                    System.out.println("Error while closing socket");
                                    Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
                             }
                        }
                        /**
                         * closes socket after finished action.
                         */
                        try {
                            if(!acceptSocket.isClosed()) acceptSocket.close();
                        } catch (IOException ex) {
                            System.out.println("Error while closing socket");
                            Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                /**
                 * receiving and sending other messages except initial
                 */
                while(!acceptSocket.isClosed())
                {
                    System.out.println("Reading incoming message");
                    
                try {
                    message=(Msg)objectReader.readObject();
                } catch (IOException ex) {
                    Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
                    
                                System.out.println("User: " + user.getUserNickname() + "Is not available!");
                                synchronized(synchoObject)
                                {
                                    if(rooms!= null && user!=null && rooms.get(user.getRoomID())!= null && !rooms.isEmpty()) rooms.get(user.getRoomID()).removeUser(user);
                                    System.out.println("User "+user.getUserNickname() + "has been removed.");
                                }
                                try {
                                acceptSocket.close();
                                
                            } catch (IOException e) {
                                Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        break; 
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                    
                    if(message.getSpecialMessage()!=null)
                    {
                        /**
                         * if user send info about disconnecting from the server
                         */
                        if(message.getSpecialMessage().equals("disconnect"))
                        {
                            System.out.println("New user disconnected!");
                            /**
                             * sending info about user disconnect from server
                             */
                            synchronized(synchoObject)
                            {
                            	System.out.println("Before deleting room " + rooms.size());
                            	
                                if(rooms!= null && user!=null && rooms.get(user.getRoomID())!= null) rooms.get(user.getRoomID()).removeUser(user);
                                if(rooms!= null && user!=null && rooms.get(user.getRoomID())!= null && rooms.get(user.getRoomID()).isEmpty())
                                    rooms.remove(user.getRoomID());
                                
                                System.out.println("After deleting room " + rooms.size());
                            }
                            if(rooms.get(user.getRoomID())!= null && !rooms.get(user.getRoomID()).isEmpty())
                                for(UserData tempUser : rooms.get(user.getRoomID()).returnUsersList())
                                {
                                try {
                                    tempUser.getUserObjectInputStream().writeObject(new Msg(null,null, null,user.getUserNickname()+" has been disconnected."));
                                } catch (IOException ex) {
                                    Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
                                    System.out.println("User: " + tempUser.getUserNickname() + "Is not available!");
                                synchronized(synchoObject)
                                {
                                    rooms.get(tempUser.getRoomID()).removeUser(tempUser);
                                    System.out.println("User "+user.getUserNickname() + "has been removed.");
                                }
                                }
                                }
                            
                            try {
                                acceptSocket.close();
                            } catch (IOException ex) {
                                Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            break;
                        }
                    }
                    else
                    {
                        /**
                             * sending message to all connected users
                             */
                            System.out.println("Sending normal message to all users!");
                            if(rooms.get(user.getRoomID())!= null)
                            for(UserData tempUser : rooms.get(user.getRoomID()).returnUsersList())
                            {
                                try
                                {
                                    System.out.println("Sending to: "+ tempUser.getUserNickname());
                                    (exceptionUser=tempUser).getUserObjectInputStream().writeObject(message);
                                } catch (IOException ex) {
                                    Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
                                    
                                    System.out.println("Deleting bad user!");
                                    /**
                                     * deleting user from room when IOException have thrown.
                                     */
                                    synchronized(synchoObject)
                                    {
                                    	
                                        if(rooms.get(message.getRoomID())!= null) rooms.get(message.getRoomID()).removeUser(exceptionUser);
                                        if(rooms.get(message.getRoomID()).isEmpty())
                                                rooms.remove(message.getRoomID());
                                    }
                                            }
                            }
                    }
                } 
        });
            threads.add(thread);
            thread.start();
          
        }
        }
    
    
    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() 
    {
        try{
                this.startServerSocket();
                this.recieveAndSendMessage();
        } catch (IOException ex) {
            canIRun=false;
            System.out.println("Error with accept!");
        }
    }
    
    
}
