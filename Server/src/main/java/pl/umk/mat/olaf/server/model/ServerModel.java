/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.server.model;


import java.io.IOException;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;

import java.net.SocketException;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import pl.umk.mat.olaf.classes.Room;
import pl.umk.mat.olaf.server.serverthread.ServerThread;


/**
 * Server Model used to manage Socket Input & Output.
 *
 * @author Aleksander
 */
public class ServerModel 
{
    
    /** The rooms. */
    private TreeMap<Integer, Room> rooms=new TreeMap<>();
    
    /** The port. */
    final private Integer port=1200; 
    
    /** The current host ip address. */
    private static String currentHostIpAddress=null;
    
    /** The current host broadcast address. */
    private static String currentHostBroadcastAddress=null;
    
    /** The server. */
    private final ServerThread server= new ServerThread(rooms, port);
    
    /**
     * Gets the current environment network ip.
     *
     * @return current server IP or localhost address if there is no another active network interface
     */
    public String getCurrentEnvironmentNetworkIp() {
        if (currentHostIpAddress == null) {
            Enumeration<NetworkInterface> netInterfaces = null;
            try {
                netInterfaces = NetworkInterface.getNetworkInterfaces();

                while (netInterfaces.hasMoreElements() && currentHostIpAddress==null) {
                    NetworkInterface ni = netInterfaces.nextElement();
                    Enumeration<InetAddress> address = ni.getInetAddresses();
                    while (address.hasMoreElements() && currentHostIpAddress==null) {
                        InetAddress addr = address.nextElement();
                    if (!addr.isLoopbackAddress() ) {
                            currentHostIpAddress = addr.getHostAddress();
                        }
                    }
                }
                if (currentHostIpAddress == null) {
                    currentHostIpAddress = "127.0.0.1";
                }

            } catch (SocketException e) {
                currentHostIpAddress = "127.0.0.1";
            }
        }
        return currentHostIpAddress;
    }
    
    /**
     * Gets the current broadcast address.
     *
     * @return current broadcast address or localhost address if there is no another active network interface
     */
    public String getCurrentEnvironmentNetworkBroadcastAddress() {
        if (currentHostBroadcastAddress == null) {
            Enumeration<NetworkInterface> netInterfaces = null;
            try {
                netInterfaces = NetworkInterface.getNetworkInterfaces();

                while (netInterfaces.hasMoreElements() && currentHostBroadcastAddress == null) {
                    NetworkInterface ni = netInterfaces.nextElement();
                    
                    if(!ni.isLoopback())
                    {
                        List<InterfaceAddress> address = ni.getInterfaceAddresses();
                        Iterator<InterfaceAddress> i = address.iterator();
                   while(i.hasNext() && currentHostBroadcastAddress == null) {
                        InterfaceAddress addr = i.next();
                    if (addr!=null ) {
                        System.out.println(addr.getBroadcast());
                        if(addr.getBroadcast().toString()!=null ) currentHostBroadcastAddress = addr.getBroadcast().toString().substring(1);
                        }
                    }
                }
                    }
                if (currentHostBroadcastAddress == null) {
                    currentHostBroadcastAddress = "127.0.0.1";
                }

            } catch (SocketException e) {
                currentHostBroadcastAddress = "127.0.0.1";
            }
        }
        return currentHostBroadcastAddress;
    }
    
    /**
     * Function retrieves Msg Object and sends it to existing/new room.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void startServerSocket() throws IOException
    {
        server.startServerSocket();
    }
    
    /**
     * Can i run.
     *
     * @return true, if successful
     */
    public boolean CanIRun()
    {
        return server.canIRun();
    }
    
    /**
     * Retrieve and send message.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void retrieveAndSendMessage() throws IOException 
    {
        server.start();
    }
    
    /**
     * function terminates all created threads and subthreads.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws InterruptedException the interrupted exception
     */
    public void terminateAllthreads() throws IOException, InterruptedException
    {
        server.terminateAllSubthreads();
        server.stopServerSocket();
        server.interrupt();
    }
}
