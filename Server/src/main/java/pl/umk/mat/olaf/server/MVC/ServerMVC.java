/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.umk.mat.olaf.server.MVC;

import pl.umk.mat.olaf.server.controller.ServerController;
import pl.umk.mat.olaf.server.model.ServerModel;
import pl.umk.mat.olaf.server.view.ServerView;

/**
 * Class used to run server application.
 *
 * @author Aleksander
 */
public class ServerMVC {
    
    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args)
	{
                 System.setProperty("java.net.preferIPv4Stack" , "true");
		ServerView theView= new ServerView();
		ServerModel theModel= new ServerModel();
			new ServerController(theModel,theView);
			theView.setVisible(true);
	}
    
}
